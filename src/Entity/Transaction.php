<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
         * @var string
         *
         * @ORM\ManyToOne(targetEntity="App\Entity\User",)
         * @ORM\JoinColumn(name="sender",referencedColumnName="id",nullable=true)
         *
         */
    private $sender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User",)
     * @ORM\JoinColumn(name="receiver",referencedColumnName="id",nullable=true)
     *
     */
    private $receiver;

    /**
     * @var string
     *
     * @ORM\Column(name="particulars", type="text")
     */
    private $particulars;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="transferid", type="string", length=255)
     */
    private $transferid;

    /**
     * @var string
     *
     * @ORM\Column(name="transactionid",type="string", length=255, nullable=true)
     */
    private $transactionid;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;


    /**
     * @var string
     *
     * @ORM\Column(name="transactiontext", type="text", nullable=true)
     */
    private $transactiontext;

    /**
     * @var bool
     *
     * @ORM\Column(name="paid", type="boolean", nullable=true)
     */
    private $paid;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted;



    public function __construct()
    {
        $this->created = new \DateTime();
        $this->deleted= 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getParticulars(): ?string
    {
        return $this->particulars;
    }

    public function setParticulars(string $particulars): self
    {
        $this->particulars = $particulars;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTransferid(): ?string
    {
        return $this->transferid;
    }

    public function setTransferid(string $transferid): self
    {
        $this->transferid = $transferid;

        return $this;
    }

    public function getTransactionid(): ?string
    {
        return $this->transactionid;
    }

    public function setTransactionid(?string $transactionid): self
    {
        $this->transactionid = $transactionid;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTransactiontext(): ?string
    {
        return $this->transactiontext;
    }

    public function setTransactiontext(?string $transactiontext): self
    {
        $this->transactiontext = $transactiontext;

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(?bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getSender(): ?User
    {
        return $this->sender;
    }

    public function setSender(?User $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }


}
