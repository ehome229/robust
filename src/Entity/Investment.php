<?php

namespace App\Entity;

use App\Repository\InvestmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvestmentRepository::class)
 */
class Investment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Clients::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $clients;

    /**
     * @ORM\ManyToOne(targetEntity=Clients::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $pack;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $CreatedDate;

    public function __construct()
    {
        $this->pack = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClients(): ?Clients
    {
        return $this->clients;
    }

    public function setClients(?Clients $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * @return Collection|Pack[]
     */
    public function getPack(): Collection
    {
        return $this->pack;
    }

    public function addPack(Pack $pack): self
    {
        if (!$this->pack->contains($pack)) {
            $this->pack[] = $pack;
            $pack->setInvestment($this);
        }

        return $this;
    }

    public function removePack(Pack $pack): self
    {
        if ($this->pack->contains($pack)) {
            $this->pack->removeElement($pack);
            // set the owning side to null (unless already changed)
            if ($pack->getInvestment() === $this) {
                $pack->setInvestment(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->CreatedDate;
    }

    public function setCreatedDate(\DateTimeInterface $CreatedDate): self
    {
        $this->CreatedDate = $CreatedDate;

        return $this;
    }

    public function setPack(?Clients $pack): self
    {
        $this->pack = $pack;

        return $this;
    }
}
