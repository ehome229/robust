<?php

namespace App\Repository;

use App\Entity\TransactionPassword;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionPassword|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionPassword|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionPassword[]    findAll()
 * @method TransactionPassword[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionPasswordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionPassword::class);
    }

    // /**
    //  * @return TransactionPassword[] Returns an array of TransactionPassword objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransactionPassword
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
