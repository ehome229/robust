<?php

namespace App\Repository;

use App\Entity\Csl;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Csl|null find($id, $lockMode = null, $lockVersion = null)
 * @method Csl|null findOneBy(array $criteria, array $orderBy = null)
 * @method Csl[]    findAll()
 * @method Csl[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CslRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Csl::class);
    }

    // /**
    //  * @return Csl[] Returns an array of Csl objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Csl
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
