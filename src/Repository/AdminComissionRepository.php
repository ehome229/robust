<?php

namespace App\Repository;

use App\Entity\AdminComission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminComission|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminComission|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminComission[]    findAll()
 * @method AdminComission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminComissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminComission::class);
    }

    // /**
    //  * @return AdminComission[] Returns an array of AdminComission objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdminComission
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
