<?php

namespace App\Repository;

use App\Entity\ClientCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientCode[]    findAll()
 * @method ClientCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientCode::class);
    }

    // /**
    //  * @return ClientCode[] Returns an array of ClientCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientCode
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
