<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\TransactionPassword;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RobustController extends AbstractController
{
    /**
     * @Route("/robust", name="dashboard")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $notifications = $em->getRepository(Notification::class)->getLatestNotification();
        
        $solde= $this->getUser()->getClients()->getCommission()->getSolde();
        return $this->render('robust/index.html.twig', [
            'notifications' => $notifications,
            'solde' => $solde,
        ]);
    }

    /**
     * @Route("/retrait", name="withdraw")
     */
    public function withdraw()
    {
        $em = $this->getDoctrine()->getManager();
        $notifications = $em->getRepository(Notification::class)->getLatestNotification();
        return $this->render('robust/withdraw.html.twig', [
            'notifications' => $notifications,
        ]);
    }

}

   
    
