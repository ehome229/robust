<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TransactionPassword;
use Symfony\Component\HttpFoundation\Request;

class TsltpswdController extends AbstractController
{
    /**
     * @Route("/creer/mot_de_passe_de_translation", name="tsltpswd")
     */
    public function index(Request $request)
    {
        //              Check to see if the user session is still active if logout
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_logout');
        }
        $session = $this->get('session');
        if ($request->isMethod('POST')) {

            $transpassword = $request->get('transpassword');
            $confirmtranspassword = $request->get('confirmtranspassword');

            $connectedUser=$this->getUser();
            $em = $this->getDoctrine()->getManager();
            $datbasePassword = $em->getRepository(TransactionPassword::class)->findOneBy([
                'user' => $connectedUser
            ]);

            if ($datbasePassword !== null) {
                $message = "Désolé! Vous avez déjà un mot de passe de translation.";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('tsltpswd');
            }

            if($transpassword!== $confirmtranspassword){
                $message = "Les mots de passe ne sont pas identiques.";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('tsltpswd');
            }

            $hashed_password = password_hash($transpassword, PASSWORD_BCRYPT);
            $tpassword= new TransactionPassword();
            $tpassword->setUser( $connectedUser)
            ->setPassword( $hashed_password)
            ->setCreatedDate(new \DateTime());

            $em->persist($tpassword);
            $em->flush();
            $message = "Les mots de passe ne sont pas identiques.";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('tsltpswd');

        }

        return $this->render('tsltpswd/index.html.twig', [
            'controller_name' => 'TsltpswdController',
        ]);
    }
}
