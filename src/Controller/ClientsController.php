<?php

namespace App\Controller;

use App\Entity\Clients;
use App\Form\ClientsType;
use App\Repository\ClientsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Commission;
use App\Entity\ClientCode;
use App\Entity\User;

/**
 * @Route("/clients")
 */
class ClientsController extends AbstractController
{
    /**
     * @Route("/", name="clients_index", methods={"GET"})
     */
    public function index(ClientsRepository $clientsRepository): Response
    {
        return $this->render('clients/index.html.twig', [
            'clients' => $clientsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="clients_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $client = new Clients();
        $form = $this->createForm(ClientsType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //dd($request);
            //$firstname= $request->get("clients")['firstname'] pour récuperer les données du form type;
            $username= $request->get("username");
            // dd($request,$username);
            $entityManager = $this->getDoctrine()->getManager();
           
            //La liaison en commission et client
            $commission= new Commission();
            $commission->setClients($client);
            $entityManager->persist($commission);
           
            //La gestion du code de chaque client genre le code qui remplace chaque client.
            $clientcode=new ClientCode();
            $rand = substr(str_shuffle('0123456789'), 0, 8);
            $clientcode->setCode('RSP'.$rand)
            ->setCreated(new \DateTime())
            ->setActivatedDate(new \DateTime())
            ->setStatus(true);
            $entityManager->persist($clientcode);
            
           // La gestion de l'encodage pour l'insertion dans la base de donnée.
            $user=new User();
            $encoded = $encoder->encodePassword($user, 'RSPpass'); 
            $user->setUsername($clientcode->getCode())
            ->setEnabled(false)
            ->setClientCode($clientcode)
            ->setRoles(['ROLE_CLIENT'])
            ->setPassword($encoded);
            $entityManager->persist($user);
            $client->setUser($user);
            $entityManager->persist($client);


            $entityManager->flush();

            return $this->redirectToRoute('clients_index');
        }

        return $this->render('clients/new.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/detaile/{id}", name="clients_show", methods={"GET"})
     */
    public function show(Clients $client): Response
    {
        return $this->render('clients/show.html.twig', [
            'client' => $client,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="clients_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Clients $client): Response
    {
        $form = $this->createForm(ClientsType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clients_index');
        }

        return $this->render('clients/edit.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/soldes", name="clients_commisons")
     */
    public function clients_commisons(): Response
    {
        $em=  $this->getDoctrine()->getManager();
        $clientCommission= $em->getRepository(Commission::class)->findBy([],['id'=> 'DESC']);
        return $this->render('clients/clientSolde.html.twig', [
            'clientCommission' => $clientCommission,
        ]);
    }


    /**
     * @Route("/delete/{id}", name="clients_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Clients $client): Response
    {
        if ($this->isCsrfTokenValid('delete'.$client->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($client);
            $entityManager->flush();
        }

        return $this->redirectToRoute('clients_index');
    }
}
