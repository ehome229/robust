<?php

namespace App\Controller;

use App\Entity\ClientCode;
use App\Entity\Transaction;
use App\Entity\TransactionPassword;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TransactionController extends AbstractController
{
    /**
     * @Route("/transaction", name="transaction")
     */
    public function index(Request $request)
    {
//              Check to see if the user session is still active if logout
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_logout');
        }
        $session = $this->get('session');
//              Check to see if the form has sent data
        if ($request->isMethod('POST')) {
//              Get the form information

            $connectedUser = $this->getUser();
            $amount = $request->get('amount');
            $reciever = $request->get('receveur');
            $transpassword = $request->get('transpassword');

//              Check if transaction password is correct
            $em = $this->getDoctrine()->getManager();
            $datbasePassword = $em->getRepository(TransactionPassword::class)->findOneBy([
                'user' => $connectedUser,
            ]);

//              If user does not have a transaction password stop transfer
            if ($datbasePassword === null) {
                $message = "Veuillez vous inscrire pour obtenir un mot de passe avant d'effectuer une transaction";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transaction');
            }
            if (!password_verify($transpassword, $datbasePassword->getPassword())) {
                $message = "Le mot de passe de transaction que vous avez saisi est incorrect";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transaction');
            }

//              Check senders balance
            $balance = $connectedUser->getClients()->getCommission()->getSolde();
            if ($amount > $balance) {
                $message = "Le montant que vous avez saisi est supérieur au solde de votre commission";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transaction');
            }

            $recieverCode = $em->getRepository(ClientCode::class)->findOneBy([
                'code' => $reciever,
            ]);
            // dd($recieverCode);
            if ($recieverCode == null) {
                $message = "Le code nexiste pas!";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transaction');
            }
            $recieverUser = $em->getRepository(User::class)->findOneBy([
                'clientCode' => $recieverCode,
            ]);
//             Record trasaction
            $particular = "";
            $transactionID = substr(str_shuffle(MD5(microtime())), 0, 20);
            $transferID = substr(str_shuffle(MD5(microtime())), 0, 15);
            $transaction = new Transaction();
            $transaction->setAmount($amount);
            $transaction->setTransactionid($transactionID);
            $transaction->setTransferid($transferID);
            $transaction->setType(8);
            $transaction->setSender($connectedUser);
            $transaction->setParticulars($particular);
            $transaction->setReceiver($recieverUser);
            $em->persist($transaction);

//                Transfer fund from one user to another

            $connectedUser->getClients()->getCommission()->setSolde($connectedUser->getClients()->getCommission()->getSolde() - (int) $amount);
            $recieverUser->getClients()->getCommission()->setSolde($recieverUser->getClients()->getCommission()->getSolde() + (int) $amount);

            $message = "Transaction effectue avec success";
            $session->getFlashBag()->add('success', $message);
            $em->flush();

            return $this->redirectToRoute('transaction');

        }
        return $this->render('transaction/index.html.twig', [
            'controller_name' => 'TransactionController',
        ]);
    }
}
