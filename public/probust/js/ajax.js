var ajax;
ajax= {
    downlinetree: function (user) {
        var loader = 'Loading please wait <i class="fas fa-sync-alt fa-spin"></i>';
        var id = '#' + user;
        var name = $('.clientname').text();
        // console.log(name,name.length);
        $(id).html(loader);
        $.ajax({
            type: "POST",
            url: Routing.generate('getBranches'),
            data: {
                user: user
            },
            success: function (data) {
                $(id).html('');
                $(id).delay(1200).append(data.response).fadeIn(1000);
                // $('.box-username').each(function(obj) {
                //     // var objetctext= $(this).text();
                //     // if(objetctext.length > 15){
                //         var substring= objetctext.substr(0, 13);
                //         console.log(objetctext);
                //         $(this).text(substring+'..');
                //     // }
                // });
            }
        });

    },
    checkTransferCode: function () {
        var sponsorcode = $('#sponsorcode').val();
        // console.log(sponsorcode);
        $('#checkintxt').css('display', 'block');
        $('#defaultText').html('Verifying code...');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkSponorCode'),
            data: {
                sponsorcode: sponsorcode
            },
            success: function (data) {
                $('#checkintxt').css('display', 'none');
                $('#defaultText').html('Sponsor '+data).fadeIn(1000);
            }
        });
    },
}